import "./stylesheets/main.css";
// import { autoUpdater } from "electron-updater";
// import log from "electron-log";

// Small helpers you might want to keep
import "./helpers/context_menu.js";
import "./helpers/external_links.js";

// ----------------------------------------------------------------------------
// Everything below is just to show you how it works. You can delete all of it.
// ----------------------------------------------------------------------------

import { remote, ipcRenderer } from "electron";
import jetpack from "fs-jetpack";
import { greet } from "./hello_world/hello_world";
import env from "env";

const app = remote.app;
const appDir = jetpack.cwd(app.getAppPath());

// log.transports.file.level = "debug";
// autoUpdater.logger = log;

// Holy crap! This is browser window with HTML and stuff, but I can read
// files from disk like it's node.js! Welcome to Electron world :)
const manifest = appDir.read("package.json", "json");

const osMap = {
  win32: "Windows",
  darwin: "macOS",
  linux: "Linux"
};

// wait for an updateReady message
ipcRenderer.on('updateReady', function(event, text) {
  console.log('I AM HERE');
    // changes the text of the button
    var container = document.createElement('div');
    container.innerHTML = "<h1>new version ready!</h1>";
    const button = document.createElement('button');
    button.innerHTML = 'Download';
    button.addEventListener('click', () => ipcRenderer.send('quitAndInstall'));
    container.appendChild(button);
    document.body.appendChild(container);
})

ipcRenderer.on('updateReady', function(event, text) {
  const container = document.createElement('h1');
  container.innerHTML = 'RECEIN' + text;
  document.body.appendChild(container);
});
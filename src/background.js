// This is main process of Electron, started as first thing when your
// app starts. It runs through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

import path from "path";
import url from "url";
import { app, Menu, ipcMain } from "electron";
import { autoUpdater } from "electron-updater";
import { devMenuTemplate } from "./menu/dev_menu_template";
import { editMenuTemplate } from "./menu/edit_menu_template";
import { helpMenuTemplate } from "./menu/help_menu_template";
import createWindow from "./helpers/window";
import Store from "electron-store";

// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from "env";

const setApplicationMenu = () => {
  const menus = [editMenuTemplate];
  if (env.name !== "production") {
    menus.push(devMenuTemplate);
  }
  menus.push(helpMenuTemplate);
  Menu.setApplicationMenu(Menu.buildFromTemplate(menus));
};


// Save userData in separate folders for each environment.
// Thanks to this you can use production and development versions of the app
// on same machine like those are two separate apps.
if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}

app.on("ready", () => {
  setApplicationMenu();

  const mainWindow = createWindow("main", {
    width: 1000,
    height: 600
  });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "app.html"),
      protocol: "file:",
      slashes: true
    })
  );

  if (env.name === "development") {
    mainWindow.openDevTools();
  }

  autoUpdater.on('update-downloaded', (info) => {
    mainWindow.webContents.send('updateReady')
  });


  // My stuff
  autoUpdater.checkForUpdates();

  const store = new Store();
  mainWindow.webContents.send('updateReady')
  console.log('HEREHERJ');
  if (store.get('runCount') === undefined) {
    store.set('runCount', 1);
    mainWindow.webContents.send('displayRoundCount', 'FIRST RUN');
  } else {
    store.set('runCount', store.get('runCount') + 1);
    mainWindow.webContents.send('displayRoundCount', store.get('runCount') + ' RUN');
  }
});


ipcMain.on("quitAndInstall", (event, arg) => {
  setImmediate(() => {
    app.removeAllListeners("window-all-closed")
    if (mainWindow != null) {
      mainWindow.close()
    }
    autoUpdater.quitAndInstall(false)
  })
});

app.on("window-all-closed", () => {
  app.quit();
});

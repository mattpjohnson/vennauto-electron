import openAboutWindow from 'about-window';

export const helpMenuTemplate = {
  label: "Help",
  submenu: [
    {
      label: "About VennAuto",
      click: () => {
        openAboutWindow({
            icon_path: '../../resources/icons/512x512.png'
        });
      }
    }
  ]
};
